import { enableProdMode } from '@angular/core';
import { environment } from './environments/environment';
import { InjectionToken } from '@angular/core';

if (environment.production) {
  enableProdMode();
}
export const REQUEST = new InjectionToken<string>('REQUEST');
export { AppServerModule } from './app/app.server.module';
