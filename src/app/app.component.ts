import { Component } from '@angular/core';
import { NewsApiService } from './news-api.service';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons/faFacebookSquare';
import { faTwitterSquare } from '@fortawesome/free-brands-svg-icons/faTwitterSquare';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SeoService } from './seo.service';

import 'rxjs/add/operator/take';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	mArticles: Array<any>;
	mSources: Array<any>;
	fbIcon = faFacebookSquare;
	pinIcon = faPinterest;
	tweetIcon = faTwitterSquare;
	whatIcon = faWhatsapp;
	public twitterData: any;
	name = '@ngx-share/buttons';
	imageurl = 'https://www.gettyimages.com/gi-resources/images/CreativeLandingPage/HP_Sept_24_2018/CR3_GettyImages-159018836.jpg';


	constructor(private newsapi: NewsApiService, public fa: FontAwesomeModule, private seo: SeoService) {
		console.log('app component constructor called');
	}

	ngOnInit() {
		//load articles
		this.newsapi.initArticles().subscribe(data => this.mArticles = data['articles']);
		//load news sources
		this.newsapi.initSources().subscribe(data => this.mSources = data['sources']);
		this.imageurl;

		this.seo.generateTags({
			title: 'UnodeCero Sports',
			description: 'La aplicación que es la polla',
			image: 'https://www.decotrend.sk/uploads/images/product/Fototapeta_Futbal_18621.jpg.large.jpg',
			url: 'http://angularssr.ml/'
		})
	}


	searchArticles(source) {
		console.log("selected source is: " + source);
		this.newsapi.getArticlesByID(source).subscribe(data => this.mArticles = data['articles']);
	}

}
